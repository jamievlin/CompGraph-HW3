""" Stack Module """
# Supakorn Rassameemasmuang


class Stack:
    """ Stack Data Type - Implementaiton of Stack ADT"""

    def __init__(self):
        self.__listitem__ = []

    def push(self, element):
        self.__listitem__.append(element)

    def pop(self):
        return self.__listitem__.pop()

    def peek(self):
        return self.__listitem__[-1]  # see slicing notation

    def is_empty(self):
        return len(self.__listitem__) == 0
    
    def __len__(self):
        return len(self.__listitem__)

    def __str__(self):
        return str(self.__listitem__)
