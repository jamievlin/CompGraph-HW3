import numpy as np
import math


class Transform:
    @classmethod
    def rotate(cls, axis, angle, radians=True):
        """Angle is supplied in degrees."""
        if radians:
            ang = angle
        else:
            ang = math.radians(angle)

        rot_mat_i = np.eye(3)
        rot_mat_o = np.matrix(np.outer(axis, axis))

        x, y, z = axis.tolist()
        rot_mat_p = np.matrix([
            [0, -z, y],
            [z, 0, -x],
            [-y, x, 0]
        ])

        rot_mat = (math.cos(ang) * rot_mat_i) + ((1 - math.cos(ang)) * rot_mat_o) + (math.sin(ang) * rot_mat_p)
        rot_mat_hom_0 = np.append(np.matrix(rot_mat), [[0, 0, 0]], 0)
        final_rot_mat = np.append(rot_mat_hom_0, np.matrix([[0, 0, 0, 1]]).getT(), 1)  # homogenize the coordinates.
        return final_rot_mat

    @classmethod
    def scale(cls, scale_size):
        x, y, z = scale_size.tolist()
        return_mat = np.matrix([
            [x, 0, 0, 0],
            [0, y, 0, 0],
            [0, 0, z, 0],
            [0, 0, 0, 1]
        ])
        return return_mat

    @classmethod
    def translate(cls, translate_coords):
        x, y, z = translate_coords.tolist()
        return_mat = np.matrix([
            [1, 0, 0, x],
            [0, 1, 0, y],
            [0, 0, 1, z],
            [0, 0, 0, 1]
        ])
        return return_mat
