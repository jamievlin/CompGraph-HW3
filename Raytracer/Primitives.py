import numpy as np
import math
from copy import deepcopy

DEBUG_FLAG = False


def mat_transform(mat, vec, homogenized=False, direction=False):
    """Assuming that vec is a 1x3 numpy array and matrix is a 4x4 homogeneous transformation matrix."""
    if not homogenized:
        vec = homogen(vec, direction)
    return dehomog((mat * np.matrix(vec).getT()), direction).A1


def dehomog(value, direction=False):
    if len(value) == 4 and not direction:
        return value[0:3] / value[3]
    elif len(value) == 4 and direction:
        return value[0:3]
    else:
        return value


def homogen(value, direction=False):
    if len(value) == 3:
        return np.append(value.flatten(), [0 if direction else 1])
    else:
        return value


def normalize(value):
    norm = np.linalg.norm(value)
    if norm > 0:
        return value / norm
    else:
        return value


def de_normalize_color(value):  # assuming linear structure.
    return tuple(int(round(255 * val)) for val in value)


def normalize_color(value):  # assuming linear color.
    return tuple(val / 255 for val in value)


class Vertex:
    def __init__(self, pos, normal=None):
        self.__position__ = pos
        self.__normal__ = normal

    @property
    def position(self):
        return self.__position__

    @position.setter
    def position(self, value):
        self.__position__ = value

    @property
    def normal(self):
        return self.__normal__


class Light:
    def __init__(self, attenuation):
        self.__color__ = np.array([1, 1, 1])
        self.__atten_params__ = np.array([1, 0, 0])
        self.__intensity__ = 1.0

    @property
    def color(self):
        return self.__color__

    @color.setter
    def color(self, value):
        self.__color__ = value

    @property
    def intensity(self):
        return self.__intensity__

    @intensity.setter
    def intensity(self, value):
        self.__intensity__ = value

    def transform(self, transformation_matrix):
        raise NotImplementedError

    def attenuation(self, distance):
        d = distance
        c, l, q = self.__atten_params__.tolist()
        return 1/(c + (l * d) + (q * d * d))

    def get_intensity(self, distance):
        return self.color * self.__intensity__ * self.attenuation(distance)


class PointLight(Light):
    def __init__(self, position, color, attenuation):
        super().__init__(attenuation)
        self.__position__ = position
        self.__color__ = color
        self.__atten_params__ = attenuation

    @property
    def position(self):
        return self.__position__

    @position.setter
    def position(self, value):
        self.__position__ = value

    def transform(self, translation_matrix):
        self.position = mat_transform(translation_matrix, self.position)


class DirectionalLight(Light):
    def __init__(self, direction, color, attenuation):
        super().__init__(attenuation)
        self.__direction__ = direction
        self.__color__ = color
        self.__atten_params__ = np.array([1, 0, 0])

    @property
    def direction(self):
        return self.__direction__

    @direction.setter
    def direction(self, value):
        self.__direction__ = value

    def transform(self, translation_matrix):
        self.direction = mat_transform(translation_matrix, self.direction, False, True)


class Material:
    def __init__(self, diffuse=np.array([1, 1, 1]), specular=np.array([0.5, 0.5, 0.5]), ambient=np.array([0, 0, 0]), emission=np.array([0, 0, 0]), shininess=1.0):
        self.__diffuse__ = diffuse
        self.__specular__ = specular
        self.__ambient__ = ambient
        self.__shinness__ = shininess
        self.__emit__ = emission

    @property
    def emission(self):
        return self.__emit__

    @emission.setter
    def emission(self, value):
        self.__emit__ = value

    @property
    def diffuse(self):
        return self.__diffuse__

    @diffuse.setter
    def diffuse(self, value):
        self.__diffuse__ = value

    @property
    def specular(self):
        return self.__specular__

    @specular.setter
    def specular(self, value):
        self.__specular__ = value

    @property
    def ambient(self):
        return self.__ambient__

    @ambient.setter
    def ambient(self, value):
        self.__ambient__ = value

    @property
    def shininess(self):
        return self.__shinness__

    @shininess.setter
    def shininess(self, value):
        self.__shinness__ = value


class GeometryObject:
    def __init__(self):
        self.__material__ = None
        if False:
            self.__material__ = Material()

    @property
    def material(self):
        return self.__material__

    def transform(self, translation_matrix):
        """Override this function.
        Transforms the object based on a 4x4 homogenous matrix"""
        raise NotImplementedError

    def collide(self, ray):
        """
        Override this function:
        Returns all t-values of the ray which objects collide with.
        """
        raise NotImplementedError
        # return -1, {}

    def normal(self, point):
        """
        Returns normal vector at that point.
        """
        raise NotImplementedError


class Sphere(GeometryObject):
    def __init__(self, center, radius, material=Material()):
        super().__init__()
        self.__center__ = center
        self.__radius__ = radius
        self.__transformation_mat__ = np.eye(4)
        self.__material__ = material

    @property
    def center(self):
        return self.__center__

    @property
    def radius(self):
        return self.__radius__

    def normal(self, point):
        # assume that point is on the (if transformed) sphere.
        inv_mat = np.linalg.pinv(self.__transformation_mat__)
        original_point = mat_transform(inv_mat, point)
        original_norm_unnormalized = normalize(original_point - self.center)

        return normalize(original_norm_unnormalized * inv_mat[0:3, 0:3]).A1

    def transform(self, translation_matrix):
        self.__transformation_mat__ = deepcopy(translation_matrix)

    def collide(self, ray):
        flags = {'tangent': False}
        # super().collide(ray)

        test_ray = ray
        if not np.array_equiv(self.__transformation_mat__, np.eye(4)):  # there's a transformation.
            inv_matrix = np.linalg.pinv(self.__transformation_mat__)  # in case of a singular matrix (necessary?)
            test_ray = ray.create_transformed_ray(inv_matrix)

        center_dehom = dehomog(self.center)
        cam_origin_minus_center = test_ray.origin - center_dehom
        a = np.linalg.norm(test_ray.direction)**2
        b = 2 * np.dot(test_ray.direction, cam_origin_minus_center)
        c = (np.linalg.norm(cam_origin_minus_center)**2) - (self.radius ** 2)

        roots = np.roots([a, b, c])  # len(roots) == 2, fundamental thm of algebra

        valid_roots = []

        for root in roots:
            if np.isreal(root) and root > 0:
                valid_roots.append(root)

        t_value = None

        if len(valid_roots) > 0:
            t_value = min(valid_roots)
            flags['point'] = mat_transform(self.__transformation_mat__, test_ray.get_point(t_value))
            if len(valid_roots) == 2:
                flags['tangent'] = (valid_roots[0] == valid_roots[1])

        return t_value, flags


class Triangle(GeometryObject):
    def __init__(self, vertices, material=Material()):
        super().__init__()
        self.__A__, self.__B__, self.__C__ = deepcopy(vertices)
        self.__material__ = material

    def transform(self, transform_mat):
        apos, bpos, cpos = homogen(self.a), homogen(self.b), homogen(self.c)

        homogenized_pos = np.matrix([
            apos.tolist(),
            bpos.tolist(),
            cpos.tolist()
        ])

        if DEBUG_FLAG:
            print('Transformed Triangle')
            print(homogenized_pos)
            print('-'*10)

        new_pos = homogenized_pos * transform_mat.getT()

        self.a = dehomog(new_pos[0].A1)
        self.b = dehomog(new_pos[1].A1)
        self.c = dehomog(new_pos[2].A1)

        if DEBUG_FLAG:
            print('Success')

        if self.a_norm is not None:
            inv_t_mat = np.linalg.pinv(transform_mat).getT()[0:3, 0:3]
            self.a_norm = (self.a_norm * inv_t_mat).A1
            self.b_norm = (self.b_norm * inv_t_mat).A1
            self.c_norm = (self.c_norm * inv_t_mat).A1

    def normal(self, point):
        if self.__A__.normal is None:
            return self.face_normal
        else:
            alpha, beta, gamma = self.__get_param_coords__(point)
            return normalize((alpha * self.a_norm) + (beta * self.b_norm) + (gamma * self.c_norm))

    @property
    def face_normal(self):
        return normalize(-1 * np.cross(self.c - self.a, self.b - self.a))

    @property
    def a(self):
        return self.__A__.position

    @a.setter
    def a(self, value):
        self.__A__.position = value

    @property
    def a_norm(self):
        return self.__A__.normal

    @property
    def b_norm(self):
        return self.__B__.normal

    @property
    def c_norm(self):
        return self.__C__.normal

    @a_norm.setter
    def a_norm(self, value):
        self.__A__.normal = value

    @b_norm.setter
    def b_norm(self, value):
        self.__B__.normal = value

    @c_norm.setter
    def c_norm(self, value):
        self.__C__.normal = value

    @property
    def b(self):
        return self.__B__.position

    @b.setter
    def b(self, value):
        self.__B__.position = value

    @property
    def c(self):
        return self.__C__.position

    @c.setter
    def c(self, value):
        self.__C__.position = value

    def __get_param_coords__(self, point):
        test_matrix = np.matrix([self.b - self.a, self.c - self.a]).getT()
        beta, gamma = np.linalg.lstsq(test_matrix, point - self.a)[0]
        alpha = 1 - beta - gamma
        return alpha, beta, gamma

    def collide(self, ray):
        t_val = None
        flags = {}
        plane_normal = self.face_normal
        n_dot_dir = np.dot(ray.direction, plane_normal)

        if n_dot_dir != 0:
            intersection_t = np.dot(self.a - ray.origin, plane_normal) / n_dot_dir
            p = ray.get_point(intersection_t)
            *args, beta, gamma = self.__get_param_coords__(p)
            # solution will always exist since p lies within
            # the triangle plane.

            if (beta >= 0) and (gamma >= 0) and (beta + gamma <= 1):
                t_val = intersection_t
                flags['point'] = ray.get_point(t_val)

        return t_val, flags

class Ray:
    @classmethod
    def create_ray(cls, camera, x, y, size):
        w, h = size
        origin_point = dehomog(camera.eye)

        w_hat = normalize(dehomog(camera.eye) - dehomog(camera.center))
        u_hat = normalize(np.cross(dehomog(camera.up), w_hat))
        v_hat = np.cross(w_hat, u_hat)

        alpha = math.tan(camera.fovx/2) * ((2 * x) / w - 1)
        beta = math.tan(camera.fovy/2) * (1 - (2 * y) / h)

        direction = normalize((alpha * u_hat) + (beta * v_hat) - w_hat)

        return Ray(origin_point, direction)

    def create_transformed_ray(self, transformation_mat):
        # new_origin_homogenized = transformation_mat * homogen(self.origin, False)
        # new_dir_homogenized = transformation_mat * homogen(self.direction, True)
        # new_origin = dehomog(new_origin_homogenized[0].A1)
        # new_dir = dehomog(new_dir_homogenized[0].A1, True)
        new_origin = mat_transform(transformation_mat, self.origin, False, False)
        new_dir = mat_transform(transformation_mat, self.direction, homogenized=False, direction=True)
        return Ray(new_origin, new_dir)

    def __init__(self, origin, direction):
        self.__origin__ = origin
        self.__direction__ = direction

    @property
    def origin(self):
        return self.__origin__

    @property
    def direction(self):
        return self.__direction__

    def get_point(self, t_val):
        return self.origin + (t_val * self.direction)
