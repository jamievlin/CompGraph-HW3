import numpy as np
import math
import matplotlib.pyplot as plt
from Transform import Transform
from multiprocessing import pool, cpu_count
from PIL import Image
from Stack import Stack
from copy import deepcopy
from datetime import datetime
from Primitives import *

DEBUG_FLAG = False


class RayTracer:
    def __init__(self):
        self.__size__ = (640, 480)
        self.__recursion_depth__ = 5
        self.__output_filename__ = 'temp_output.png'
        self.__objects__ = []
        self.__lights__ = []
        self.__camera__ = None
        self.__enable_log__ = True
        self.__final_image__ = Image.new('RGB', self.__size__)
        self.__transform_stack__ = Stack()
        self.__transform_stack__.push(np.eye(4))
        self.__current_transform__ = self.__transform_stack__.peek()
        self.__enable_reflections__ = True
        self.__epsilon__ = 0.0015
        self.__enable_partial_render__ = False
        self.__pixel_range__ = (0, 200), (0, 200)

    @property
    def enable_reflection(self):
        return self.__enable_reflections__

    @enable_reflection.setter
    def enable_reflection(self, value):
        self.__enable_reflections__ = value

    @property
    def partial_render(self):
        return self.__enable_partial_render__

    @property
    def pixel_range(self):
        return self.__pixel_range__

    @pixel_range.setter
    def pixel_range(self, value):
        self.__pixel_range__ = value

    @partial_render.setter
    def partial_render(self, value):
        self.__enable_partial_render__ = value

    def push_transform(self):
        self.__transform_stack__.push(self.__current_transform__)

    def pop_transform(self):
        self.__current_transform__ = self.__transform_stack__.pop()

    def add_translate(self, axis):
        self.__current_transform__ = self.__current_transform__ * Transform.translate(axis)

    def add_rotate(self, axis, degrees):
        self.__current_transform__ = self.__current_transform__ * Transform.rotate(axis, degrees, radians=False)

    def add_scale(self, axis):
        self.__current_transform__ = self.__current_transform__ * Transform.scale(axis)

    def add_object(self, obj):
        assert isinstance(obj, GeometryObject)
        obj.transform(self.__current_transform__)
        self.__objects__.append(obj)

    def add_light(self, light):
        assert isinstance(light, Light)
        light.transform(self.__current_transform__)
        self.__lights__.append(light)

    @property
    def output_file(self):
        return self.__output_filename__

    @output_file.setter
    def output_file(self, value):
        self.__output_filename__ = value

    @property
    def recursion_depth(self):
        return self.__recursion_depth__

    @recursion_depth.setter
    def recursion_depth(self, value):
        self.__recursion_depth__ = value

    @property
    def size(self):
        return self.__size__

    @size.setter
    def size(self, value):
        self.__size__ = value

    @property
    def camera(self):
        return self.__camera__

    @camera.setter
    def camera(self, value):
        self.__camera__ = value

    @property
    def objects(self):
        return self.__objects__

    @property
    def final_image(self):
        return self.__final_image__

    # def __raytrace__(self):
    #    output_image = None
    #    for x in range(self.size[0]):
    #        for y in range(self.size[1]):
    #            self.__raytracepixel__(x, y)
    #    return output_image

    def __mtraytrace__(self):
        pxl_list = [(i, j) for i in range(self.size[0]) for j in range(self.size[1])]
        if self.__enable_partial_render__:
            x_range, y_range = self.__pixel_range__
            x_min, x_max = x_range
            y_min, y_max = y_range
            pxl_list = [p for p in pxl_list if x_min <= p[0] < x_max and y_min <= p[1] < y_max]

        num_cores = cpu_count()
        p = pool.Pool(num_cores)
        result = p.map(self.__ray_trace_pixel__, pxl_list)
        img_out = Image.new('RGB', self.size)
        for pixel_index in range(len(pxl_list)):
            img_out.putpixel(pxl_list[pixel_index], result[pixel_index])
        return img_out

    def __ray_trace_pixel__(self, pixel):
        i, j = pixel
        camera_ray = Ray.create_ray(self.camera, i + 0.5, j + 0.5, self.size)
        pixel_color, *args = self.__trace_ray__(camera_ray, 1)

        if self.__enable_log__:
            print(str(datetime.now()), 'Trace at pixel', str(pixel), 'successful. Color:', str(pixel_color))

        assert len(pixel_color) == 3
        return de_normalize_color(pixel_color)

    def __trace_ray__(self, ray, depth):
        min_collision = np.Infinity
        collided_object = None
        collided_flags = None
        pixel_color = (0, 0, 0)
        raw_color = np.array([0, 0, 0])

        for geom_object in self.__objects__:
            t_value, flags = geom_object.collide(ray)

            if t_value is not None:  # collision
                if self.__epsilon__ < t_value < min_collision:
                    min_collision = t_value
                    collided_object = geom_object
                    collided_flags = flags

        if collided_object is not None:
            pixel_color, raw_color = self.__calculate_light__(None, ray, collided_object, min_collision, collided_flags, depth)

        return pixel_color, collided_object, min_collision, collided_flags, raw_color

    def __calculate_light__(self, pixel, ray, obj, t_value, flags, depth):
        assert obj is not None
        base_color = np.array([0, 0, 0]) + np.array(obj.material.emission) + np.array(obj.material.ambient)
        collide_point = ray.get_point(t_value)
        face_normal = obj.normal(collide_point)

        for light in self.__lights__:
            visible, vec_dir = self.__get_light_col_data__(collide_point, light)
            if visible == 1:
                diffuse_color = obj.material.diffuse * max(face_normal @ normalize(-1 * vec_dir), 0)

                half_ang = normalize(normalize(-1 * vec_dir) + normalize(-1 * ray.direction))
                spec_color = obj.material.specular * math.pow(max(half_ang @ face_normal, 0),
                                                              obj.material.shininess)

                light_contribution = light.get_intensity(np.linalg.norm(vec_dir)) * (
                    diffuse_color + spec_color)
                base_color = base_color + light_contribution

        if self.enable_reflection and depth <= self.recursion_depth and -(face_normal @ ray.direction) >= 0 and \
                obj.material.specular.any():
            new_dir = ray.direction - (2 * (face_normal @ ray.direction) * face_normal)
            *args, raw_refl = self.__trace_ray__(Ray(collide_point, new_dir), depth + 1)
            refl_color = obj.material.specular * raw_refl
            base_color = base_color + refl_color

            if self.__enable_log__:
                print(' '*depth, 'Recursive Trace success. Color:', refl_color)

        return tuple(np.clip(base_color, 0.0, 1.0).tolist()), base_color

    def __get_light_col_data__(self, point, light):
        """Returns visibility, vector/direction of light"""
        visible = 1
        if isinstance(light, PointLight):
            vec_dir = point - light.position
            t_value = np.linalg.norm(vec_dir)
            ray_dir = normalize(vec_dir)
        else:  # directional light
            ray_dir = normalize(light.direction)
            t_value = np.Inf
            vec_dir = light.direction
        # point = point + (epsilon * ray_dir)
        test_ray = Ray(point, -1 * ray_dir)

        for obj in self.__objects__:
            t_val_result, *args = obj.collide(test_ray)
            if t_val_result is not None:
                if self.__epsilon__ < t_val_result < t_value:
                    visible = 0
                    break

        return visible, vec_dir

    def trace(self):
        output_image = self.__mtraytrace__()
        print('Success.')
        self.__final_image__ = output_image

    def show_image(self):
        if self.final_image is not None:
            plt.imshow(self.final_image)
            plt.show()
        else:
            print('Not yet rendered!')


class Camera:
    def __init__(self, eye, center, up, fovy, parent_raytracer):
        self.__eye__ = eye
        self.__center__ = center
        self.__up__ = up
        self.__fovy__ = math.radians(fovy)
        self.__parentraytracer__ = parent_raytracer

    @property
    def fovy(self):
        return self.__fovy__

    @property
    def fovx(self):
        x, y = self.__parentraytracer__.size
        aspect_ratio = x / y
        return 2 * math.atan(aspect_ratio * math.tan(self.fovy / 2))

    @fovy.setter
    def fovy(self, value):
        """In Radians."""
        self.__fovy__ = math.radians(value)

    @property
    def eye(self):
        return self.__eye__

    @eye.setter
    def eye(self, value):
        self.__eye__ = value

    @property
    def up(self):
        return self.__up__

    @up.setter
    def up(self, value):
        self.__up__ = value

    @property
    def center(self):
        return self.__center__

    @center.setter
    def center(self, value):
        self.__center__ = value


