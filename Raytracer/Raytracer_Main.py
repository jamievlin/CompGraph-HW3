import sys
import io
import numpy as np
from Raytracer import *
from Primitives import *


def main(args):
    params = {
        'enable_reflection': True,
        'internal_scale': 1,
        'show_img': True,
        'partial_render': False
    }
    vertices_index = []
    vertices_w_normal_index = []
    current_attenuation = np.array([1, 0, 0])
    current_material = Material()

    if len(args) == 0:
        default_file = 'hw3-submissionscenes/scene5.test'
        print('Default:', default_file)
        input_file = input('Enter input file (or leave blank for default): ')
        if not input_file:
            input_file = default_file
            print('Defaulting to', default_file)
    else:
        input_file = args[0]
        print(str.format('Input File: {0}', input_file))

    for i in range(1, len(args)):
        argument = args[i]
        if argument == '-noshowimg':
            params['show_img'] = False
        elif argument == '-internalscale':
            params['internal_scale'] = float(args[i+1])
        elif argument == '-disablereflections':
            params['enable_reflection'] = False
        elif argument == '-partialrender':
            params['partial_render'] = True
            params['pixel_range_x'] = tuple([int(i) for i in args[i + 1:i + 3]])
            params['pixel_range_y'] = tuple([int(i) for i in args[i + 3:i + 5]])

    print(params)

    try:
        scene_file = io.open(input_file, 'r')
        scene_lines = scene_file.read().splitlines()
        scene_file.close()
    except IOError:
        print("Oops. IOError.")
        raise

    rt1 = RayTracer()
    rt1.enable_reflection = params['enable_reflection']
    rt1.partial_render = params['partial_render']
    if params['partial_render']:
        rt1.pixel_range = params['pixel_range_x'], params['pixel_range_y']

    for line in scene_lines:
        assert isinstance(line, str)
        if (not line.startswith('#')) and line:
            cmd_array = line.split()
            cmd = cmd_array[0]
            cmd_params = cmd_array[1:]

            # Base Setup
            if cmd == 'size':
                rt1.size = tuple(round(int(pos) * params['internal_scale']) for pos in cmd_params[0:2])
            elif cmd == 'maxdepth':
                rt1.recursion_depth = int(cmd_params[0])
            elif cmd == 'output':
                rt1.output_file = cmd_params[0]
            elif cmd == 'camera':
                eye_vec = np.array([float(pos) for pos in cmd_params[0:3]])
                center_vec = np.array([float(pos) for pos in cmd_params[3:6]])
                up_vec = np.array([float(pos) for pos in cmd_params[6:9]])
                fovy = float(cmd_params[9])
                rt1.camera = Camera(eye_vec, center_vec, up_vec, fovy, rt1)

            elif cmd == 'sphere':
                center = np.array([float(pos) for pos in cmd_params[0:3]])
                radius = float(cmd_params[3])
                rt1.add_object(Sphere(center, radius, deepcopy(current_material)))

            # Vertices
            elif cmd == 'vertex':
                new_vertex_pos = np.array([float(pos) for pos in cmd_params[0:3]])
                vertices_index.append(Vertex(new_vertex_pos))
            elif cmd == 'vertexnormal':
                new_vertex_pos = np.array([float(pos) for pos in cmd_params[0:3]])
                new_normal = np.array([float(pos) for pos in cmd_params[3:6]])
                vertices_w_normal_index.append(Vertex(new_vertex_pos, new_normal))

            # Triangles
            elif cmd == 'tri':
                vertices_list = []
                for i in range(3):
                    index_val = int(cmd_params[i])
                    vertices_list.append(vertices_index[index_val])
                rt1.add_object(Triangle(vertices_list, deepcopy(current_material)))
            elif cmd == 'trinormal':
                vertices_list = []
                for i in range(3):
                    index_val = int(cmd_params[i])
                    vertices_list.append(vertices_w_normal_index[index_val])
                rt1.add_object(Triangle(vertices_list, deepcopy(current_material)))

            # Transformations
            elif cmd == 'pushTransform':
                rt1.push_transform()
            elif cmd == 'popTransform':
                rt1.pop_transform()
            elif cmd == 'rotate':
                axis = np.array([float(pos) for pos in cmd_params[0:3]])
                angle = float(cmd_params[3])
                rt1.add_rotate(axis, angle)
            elif cmd == 'translate':
                axis = np.array([float(pos) for pos in cmd_params[0:3]])
                rt1.add_translate(axis)
            elif cmd == 'scale':
                axis = np.array([float(pos) for pos in cmd_params[0:3]])
                rt1.add_scale(axis)

            # Lights
            elif cmd == 'directional':
                direction = np.array([float(pos) for pos in cmd_params[0:3]])
                col = np.array([float(pos) for pos in cmd_params[3:6]])
                rt1.add_light(DirectionalLight(direction, col, deepcopy(current_attenuation)))
            elif cmd == 'point':
                pos = np.array([float(pos) for pos in cmd_params[0:3]])
                col = np.array([float(pos) for pos in cmd_params[3:6]])
                rt1.add_light(PointLight(pos, col, deepcopy(current_attenuation)))
            elif cmd == 'attenuation':
                current_attenuation = np.array([float(pos) for pos in cmd_params[0:3]])
            elif cmd == 'ambient':
                current_material.ambient = np.array([float(pos) for pos in cmd_params[0:3]])

            # Materials
            elif cmd == 'diffuse':
                current_material.diffuse = np.array([float(pos) for pos in cmd_params[0:3]])
            elif cmd == 'specular':
                current_material.specular = np.array([float(pos) for pos in cmd_params[0:3]])
            elif cmd == 'emission':
                current_material.emission = np.array([float(pos) for pos in cmd_params[0:3]])
            elif cmd == 'shininess':
                current_material.shininess = float(cmd_params[0])

    print('Starting Raytrace...')
    rt1.trace()
    print('Raytrace Success!')
    rt1.final_image.save(rt1.output_file)
    if params['show_img']:
        rt1.show_image()

if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]) or 0)
